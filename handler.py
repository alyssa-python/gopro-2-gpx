#
# 2021-04-30
#
# based on code from Juan M. Casillas <juanm.casillas@gmail.com>:
#   https://github.com/juanmcasillas/gopro2gpx.git
#

from pathlib import Path
import urllib.parse
import os
import boto3
import ffmpeg
import array
import sys
import json

from klvdata import KLVData
import fourCC
import gpshelper
from datetime import datetime
import time

s3 = boto3.resource('s3')
# s3 = boto3.client('s3')

# TODO https://pypi.org/project/python-dotenv/
PATH_TO_FFMPEG = "/opt/bin/ffmpeg"
PATH_TO_FFPROBE = "/opt/bin/ffprobe"

def extract(event, context):
    """
    From S3 Event: 
        download MP4
        extract GPMF
        parse GPX data
        Put in S3
        delete from EFS
    """
    record = event["Records"][0]["s3"]
    bucket = record['bucket']['name']
    # key = record["object"]["key"] 
    key = urllib.parse.unquote_plus(record['object']['key'], encoding='utf-8')
    file_name = Path(key).name
    file_stem = Path(key).stem
    file_suffix = Path(key).suffix.lower()
    file_path = f'/mnt/originals/{file_name}'
    f = Path(file_path)
    tmp_out_file = "/tmp/%s.gpx" % file_stem
    out_file = f"data/{file_stem}.gpx"

    # only process MP4 files (to prevent endless loop)
    if file_suffix != '.mp4': # or .mov
        return record

    if f.exists():
        print(f'cleaning up {file_path}')
        os.remove(file_path)

    try:
        # obj = s3.get_object(Bucket=bucket, Key=key)
        # # print(obj)

        # body = obj['Body'].read()
        # print("===== raw_metadata")
        # # <botocore.response.StreamingBody object at 0x7f79151a9dc0>
        # print(body)
        # print("===== end raw_metadata")

        s3.Object(bucket, key).download_file(file_path)
        # s3.download_file(bucket, key, file_path)
        # with open(file_path, 'wb') as data:
        #     s3.download_fileobj(bucket, key, data)

        md = getMetadata(file_path)
        t_id = getMetadataTrack(md)
        raw_metadata = getGpmf(file_path, t_id)
        # print("===== raw_metadata")
        # print(raw_metadata)
        # print("===== end raw_metadata")

        # TODO: TypeError: a bytes-like object is required, not 'NoneType'
        data = parse_stream(raw_metadata)
        # print("===== data")
        # print(data)
        # print("===== end data")
        points = BuildGPSPoints(data)
        # print("===== points")
        # print(points)
        # print("===== end points")
        gpx = gpshelper.generate_GPX(points, trk_name="gopro7-track")

        with open(tmp_out_file , "w+") as fd:
            fd.write(gpx)

        s3.Bucket(bucket).upload_file(tmp_out_file, out_file)
        # with open(tmp_out_file, "rb") as f:
        #     s3.put_object(f, bucket, out_file)
            
        # delete from EFS
        os.remove(file_path)

        # TODO: delete from Bucket?
        # s3.Object(bucket, key).delete()

    except:
        tmp_error_file = "/tmp/%s-error.txt" % file_stem
        (typ, value, traceback) = sys.exc_info()

        # with open(tmp_error_file, "w+") as fd:
        #     # write() argument must be str, not Error/Traceback
        #     fd.write(value)

        # s3.Bucket(bucket).upload_file(tmp_error_file, out_file)
        # with open(tmp_out_file, "rb") as f:
        #     s3.put_object(f, bucket, out_file)

        print(value)
        print(traceback)

        return {
            "type": typ.__name__,
            # "value": str(value),
            # "traceback": traceback #.print_tb()
        }

    return {
        # "bucket": bucket,
        # "key": key,
        "input": file_name,
        # "output": s3.generate_presigned_url('get_object',
        #                                             Params={'Bucket': bucket,
        #                                                     'Key': out_file}),
        # "metadata": md,
        # "tracks": md["streams"],
        # "track_number": f'0:{t_id}',
        # "track": md["streams"][t_id],
        # "raw": raw_metadata # Binary data,
        # "data": parse_stream(raw_metadata), # not JSON serializable
        # "points": json.dumps(gpx),
        "gpx": gpshelper.generate_GeoJSON(points),
    }

def getMetadata(fname):
    """ ffprobe -print_format json -show_streams <PATH_TO_MP4> """
    return ffmpeg.probe(fname, cmd=PATH_TO_FFPROBE) # , print_format='json', show_streams=True

def getMetadataTrack(md):
    """ find index of track where codec_tag_string = gpmd """

    # return [stream for stream in md['streams'] if stream['codec_tag_string']=="gpmd"]["index"]
    # print([stream for stream in md['streams'] if stream['codec_tag_string']=="gpmd"]["index"])

    stream = next((stream for stream in md['streams'] if stream['codec_tag_string'] == 'gpmd'), None)

    if not stream:
        return(None)
    return(int(stream["index"]))

def getGpmf(fname, track):
    """ get raw metadata stream as buffer """
    out, _ = (
        ffmpeg
            .input(fname)
            .output('pipe:', format='rawvideo', codec='copy', map=f"0:{track}") 
            .overwrite_output()
            # .get_args()
            .run(capture_stdout=True, cmd=PATH_TO_FFMPEG)
        )

    # f = open("%s.bin" % outputfile, "wb")
    # f.write(out)
    # f.close()

    # print(out)
    return out
    
def parse_stream(raw_data):
    """ Main code that extracts GPS points """
    data = array.array('b')
    data.frombytes(raw_data)

    offset = 0
    klvlist = []

    while offset < len(data):

        klv = KLVData(data,offset)
        if not klv.skip():
            klvlist.append(klv)
            # if self.verbose == 3:
            #     print(klv)
        else:
            if klv: 
                print("Warning, skipping klv", klv)
            else:
                # unknown label
                pass
                
        offset += 8
        if klv.type != 0:
            offset += klv.padded_length
            #print(">offset:%d length:%d padded:%d" % (offset, length, padded_length))

    # print(klvlist)
    return(klvlist)

def BuildGPSPoints(data, skip=False):
    """
    Data comes UNSCALED so we have to do: Data / Scale.
    Do a finite state machine to process the labels.
    GET
     - SCAL     Scale value
     - GPSF     GPS Fix
     - GPSU     GPS Time
     - GPS5     GPS Data
    """

    points = []
    SCAL = fourCC.XYZData(1.0, 1.0, 1.0)
    GPSU = None
    SYST = fourCC.SYSTData(0, 0)

    stats = {
        'ok': 0,
        'badfix': 0,
        'badfixskip': 0,
        'empty' : 0
    }

    GPSFIX = 0 # no lock.
    for d in data:
        
        if d.fourCC == 'SCAL':
            SCAL = d.data
        elif d.fourCC == 'GPSU':
            GPSU = d.data
        elif d.fourCC == 'GPSF':
            if d.data != GPSFIX:
                print("GPSFIX change to %s [%s]" % (d.data,fourCC.LabelGPSF.xlate[d.data]))
            GPSFIX = d.data
        elif d.fourCC == 'GPS5':
            # we have to use the REPEAT value.

            for item in d.data:

                if item.lon == item.lat == item.alt == 0:
                    print("Warning: Skipping empty point")
                    stats['empty'] += 1
                    continue

                if GPSFIX == 0:
                    stats['badfix'] += 1
                    if skip:
                        print("Warning: Skipping point due GPSFIX==0")
                        stats['badfixskip'] += 1
                        continue

                retdata = [ float(x) / float(y) for x,y in zip( item._asdict().values() ,list(SCAL) ) ]
                

                gpsdata = fourCC.GPSData._make(retdata)
                p = gpshelper.GPSPoint(gpsdata.lat, gpsdata.lon, gpsdata.alt, datetime.fromtimestamp(time.mktime(GPSU)), gpsdata.speed)
                points.append(p)
                stats['ok'] += 1

        elif d.fourCC == 'SYST':
            data = [ float(x) / float(y) for x,y in zip( d.data._asdict().values() ,list(SCAL) ) ]
            if data[0] != 0 and data[1] != 0:
                SYST = fourCC.SYSTData._make(data)


        elif d.fourCC == 'GPRI':
            # KARMA GPRI info

            if d.data.lon == d.data.lat == d.data.alt == 0:
                print("Warning: Skipping empty point")
                stats['empty'] += 1
                continue

            if GPSFIX == 0:
                stats['badfix'] += 1
                if skip:
                    print("Warning: Skipping point due GPSFIX==0")
                    stats['badfixskip'] += 1
                    continue

            data = [ float(x) / float(y) for x,y in zip( d.data._asdict().values() ,list(SCAL) ) ]
            gpsdata = fourCC.KARMAGPSData._make(data)

            if SYST.seconds != 0 and SYST.miliseconds != 0:
                p = gpshelper.GPSPoint(gpsdata.lat, gpsdata.lon, gpsdata.alt, datetime.fromtimestamp(SYST.miliseconds), gpsdata.speed)
                points.append(p)
                stats['ok'] += 1




    print("-- stats -----------------")
    total_points =0
    for i in stats.keys():
        total_points += stats[i]
    print("- Ok:              %5d" % stats['ok'])
    print("- GPSFIX=0 (bad):  %5d (skipped: %d)" % (stats['badfix'], stats['badfixskip']))
    print("- Empty (No data): %5d" % stats['empty'])
    print("Total points:      %5d" % total_points)
    print("--------------------------")
    # print(points)
    return(points)