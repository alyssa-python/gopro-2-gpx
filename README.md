# GoPro 2 GPX

[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/alyssa-python/gopro-2-gpx)

## Getting Started

### Prerequisites

**ENV variables**

- [SERVERLESS_ACCESS_KEY](https://www.serverless.com/framework/docs/guides/cicd/running-in-your-own-cicd#create-an-access-key-in-the-serverless-framework-dashboard)
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY

**AWS**

- VPC
  + VPC Endpoint
- EFS
- [FFMPEG Lambda Layer](https://serverlessrepo.aws.amazon.com/applications/us-east-1/145266761615/ffmpeg-lambda-layer)

**Deploy**
1. `yarn`
1. `yarn deploy`

-----

Thanks to [Juan M. Casillas](mailto:juanm.casillas@gmail.com) for https://github.com/juanmcasillas/gopro2gpx.git